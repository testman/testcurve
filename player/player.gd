extends CharacterBody2D

var rotation_speed = 0.1
var movement_speed = 2
var direction
var player_trail
var trail_line
var previous_position

var left
var right

func _ready():
	player_trail = get_tree().get_first_node_in_group(&"player_trail")
	trail_line = get_tree().get_first_node_in_group(&"trail_line")
	previous_position = $trail_spawner.global_position

func _physics_process(delta):
	left = Input.is_action_pressed(&"ui_left")
	right = Input.is_action_pressed(&"ui_right")
	# holding down both buttons should do nothing
	if left or right:
		if left:
			rotate(-rotation_speed)
		if right:
			rotate(rotation_speed)
	
	direction = Vector2(0, -movement_speed).rotated(rotation)
	var collision = move_and_collide(direction)
	if collision:
		player_trail.queue_free()
		queue_free()
	trail_line.add_point($trail_spawner.global_position)
	var new_collision_segment = StaticBody2D.new()
	var new_collision_shape = CollisionShape2D.new()
	var new_segment = SegmentShape2D.new()
	new_segment.a = previous_position
	new_segment.b = $trail_spawner.global_position
	new_collision_shape.shape = new_segment
	new_collision_segment.add_child(new_collision_shape)
	player_trail.add_child(new_collision_segment)
	previous_position = $trail_spawner.global_position
	
